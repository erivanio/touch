from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.checkin.models import *
from apps.locacao.models import *
from apps.lojas.models import *
from apps.restaurantes.models import *
from apps.slides.models import Slides
from apps.turismo.models import *

def home(request):
    slides = Slides.objects.filter(publicado__exact=True)
    carros = Carro.objects.filter(disponivel__exact=1)
    restaurantes = Restaurantes.objects.all()
    lojas = Lojas.objects.all()
    lugares = Lugar.objects.all()
    companhias = Checkin.objects.all()
    return render_to_response('tservice/index.html', locals(), context_instance=RequestContext(request))
