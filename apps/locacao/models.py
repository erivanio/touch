from django.db import models
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField

class Locadora(models.Model):

    nome = models.CharField('Nome', max_length=100)
    logo = ThumbnailerImageField(upload_to='uploads', null=True, blank=True)
    thumb = ImageRatioField('logo', '150x150')

    class Meta:
        verbose_name = 'Locadora'
        verbose_name_plural = 'Locadoras'

    def imagemAdmin(self):
        if self.logo:
            im = get_thumbnailer(self.logo).get_thumbnail({
                'size': (150, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Logo'

    def __unicode__(self):
        return self.nome

class Cliente(models.Model):

    nome = models.CharField(max_length=200)
    telefone = models.CharField(max_length=14)

    def __unicode__(self):
        return self.nome

class Carro(models.Model):

    nome = models.CharField('Modelo', max_length=100)
    foto = ThumbnailerImageField(upload_to='uploads', null=True, blank=True)
    thumb = ImageRatioField('foto', '150x150')
    valor = models.FloatField(help_text='Coloque o valor separando os centavos por "." ex.: 150.50')
    disponivel = models.BooleanField(default=True)

    locadora = models.ForeignKey(Locadora)
    cliente = models.ForeignKey(Cliente)

    class Meta:
        verbose_name = 'Carro'
        verbose_name_plural = 'Carros'

    def imagemAdmin(self):
        if self.foto:
            im = get_thumbnailer(self.foto).get_thumbnail({
                'size': (150, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'
    imagemAdmin.is_safe = True
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Foto'

    def __unicode__(self):
        return self.nome

