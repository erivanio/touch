# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.locacao.models import Carro, Locadora, Cliente
from image_cropping import ImageCroppingMixin

class CarroInline(ImageCroppingMixin, admin.StackedInline):
    model = Carro
    extra = 3
    #list_display = ('imagemAdmin', 'nome', 'disponivel')
    #list_filter = ['nome', 'disponivel']
    #search_fields = ['nome', 'disponivel']

class LocadoraAdmin(ImageCroppingMixin, admin.ModelAdmin):
    inlines = [CarroInline]
    list_display = ('imagemAdmin', 'nome')
    list_filter = ['nome']
    search_fields = ['nome']

class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nome', 'telefone')
    search_fields = ['nome']

admin.site.register(Locadora, LocadoraAdmin)
admin.site.register(Cliente, ClienteAdmin)