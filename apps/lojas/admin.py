# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.lojas.models import Lojas
from image_cropping import ImageCroppingMixin

class LojasAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('imagemAdmin', 'nome', 'tipo')
    list_filter = ['nome', 'tipo']
    search_fields = ['nome', 'tipo']

admin.site.register(Lojas, LojasAdmin)