# -*- coding: utf-8 -*-
from django.db import models
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField


class Lojas(models.Model):

    nome = models.CharField('Nome da Loja', max_length=100)
    logo = ThumbnailerImageField(upload_to='uploads', null=True, blank=True)
    thumb = ImageRatioField('logo', '150x150')
    tipo = models.CharField('Tipo de Loja', max_length=100)
    link = models.TextField('Localização', help_text="Colocar o link do google maps.")

    class Meta:
        verbose_name = 'Loja'
        verbose_name_plural = 'Lojas'

    def imagemAdmin(self):
        if self.logo:
            im = get_thumbnailer(self.logo).get_thumbnail({
                'size': (150, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Logo'

    def __unicode__(self):
        return self.nome