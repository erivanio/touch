# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.restaurantes.models import Restaurantes
from image_cropping import ImageCroppingMixin

class RestaurantesAdmin(ImageCroppingMixin,admin.ModelAdmin):
    list_display = ('imagemAdmin', 'nome', 'tipo')
    list_filter = ['nome', 'tipo']
    search_fields = ['nome', 'tipo']

admin.site.register(Restaurantes, RestaurantesAdmin)