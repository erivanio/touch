from django.http import HttpResponseRedirect
from apps.taxi.forms import TaxiForm

def pedidoTaxi(request):

    form = TaxiForm()
    if request.method == 'POST':
        form = TaxiForm(request.POST)

    if form.is_valid():
        form.save()
    return HttpResponseRedirect('/', locals())
