from django import forms
from apps.taxi.models import Taxi

class TaxiForm(forms.ModelForm):

    class Meta:
        model = Taxi
        fields = ('nome', 'telefone')
