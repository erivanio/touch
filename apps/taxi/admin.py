# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.taxi.models import *

class TaxiAdmin(admin.ModelAdmin):
    list_display = ('nome', 'telefone')
    search_fields = ['nome']

admin.site.register(Taxi, TaxiAdmin)