from datetime import datetime
from django.db import models


class Taxi(models.Model):

    nome = models.CharField('Nome', max_length=100)
    telefone = models.CharField(max_length=14)
    dt_pedido = models.DateTimeField('Data do Pedido', default=datetime.now)

    class Meta:
        verbose_name = 'Pedido de Taxi'
        verbose_name_plural = 'Pedidos de Taxi'

    def __unicode__(self):
        return self.nome
