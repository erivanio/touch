# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.checkin.models import Checkin
from image_cropping import ImageCroppingMixin

class CheckinAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('imagemAdmin', 'nome')
    list_filter = ['nome']
    search_fields = ['nome']

admin.site.register(Checkin, CheckinAdmin)