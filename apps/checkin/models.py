# -*- coding: utf-8 -*-
from django.db import models
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField

class Checkin(models.Model):

    nome = models.CharField('Companhia Aerea', max_length=100)
    logo = ThumbnailerImageField(upload_to='uploads', null=True, blank=True)
    thumb = ImageRatioField('logo', '326x141')
    link = models.TextField(help_text="Link da página de check-in da companhia aérea.")

    class Meta:
        verbose_name = 'Companhia de Vôo'
        verbose_name_plural = 'Companhias de Vôos'

    def get_thumb(self):
        return get_thumbnailer(self.logo).get_thumbnail({
            'size': (326, 141),
            'box': self.thumb,
            'crop': True,
            'detail': True,
            }).url

    def imagemAdmin(self):
        if self.logo:
            im = get_thumbnailer(self.logo).get_thumbnail({
                'size': (150, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'
    imagemAdmin.is_safe = True
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Logo'

    def __unicode__(self):
        return self.nome