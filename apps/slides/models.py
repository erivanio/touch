from django.db import models
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField


class Slides(models.Model):
    imagem = ThumbnailerImageField(upload_to='uploads')
    thumb = ImageRatioField('imagem', '200x150')
    legenda = models.CharField(max_length=200)
    publicado = models.BooleanField(default=True)

    class Meta:
        verbose_name="Inserir Slideshow"
        verbose_name_plural="Inserir Slideshow"

    def imagemAdmin(self):
        if self.imagem:
            im = get_thumbnailer(self.imagem).get_thumbnail({
                'size': (200, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'
    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Imagem'

    def __unicode__(self):
        return self.legenda
