# -*- coding: utf-8 -*-
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from apps.slides.models import Slides


class SlidesAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('imagemAdmin', 'legenda', 'publicado')
    search_fields = ['legenda', 'publicado']
    list_filter = ['publicado']

admin.site.register(Slides, SlidesAdmin)