# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField


class Lugar(models.Model):

    nome = models.CharField(max_length=200)
    imagem = ThumbnailerImageField('Foto', upload_to='uploads')
    thumb = ImageRatioField('imagem', '150x150')
    link = models.TextField('Localização', help_text="Colocar o link do google maps.")

    class Meta:
        verbose_name = 'Ponto Turistico'
        verbose_name_plural = 'Pontos Turisticos'

    def imagemAdmin(self):
        if self.imagem:
            im = get_thumbnailer(self.imagem).get_thumbnail({
                'size': (150, 150),
                'box': self.thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'

    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Foto'

    def __unicode__(self):
        return self.nome

class Foto(models.Model):

    foto = models.ImageField(upload_to='uploads/')#, size=(530, 397)
    legenda = models.CharField(max_length=200, blank=True)
    foto_thumb = ImageRatioField('foto','142x100')
    lugar = models.ForeignKey(Lugar)
    cadastrado_em = models.DateTimeField(verbose_name='Data do Cadastro', default=datetime.now, blank=True, null = True)
    status = models.BooleanField(verbose_name='Ativo? ', default=True)

    class Meta:
        ordering = ('-cadastrado_em',)

    def imagemAdmin(self):
        if self.foto:
            im = get_thumbnailer(self.foto).get_thumbnail({
                'size': (135, 90),
                'box': self.foto_thumb
            })
            return '<img src="{0}" />'.format(im.url)
        else:
            return 'Sem Imagem'

    def imagem(self):
        if self.foto:
            im = get_thumbnailer(self.foto).get_thumbnail({
                'size': (135, 90),
                'box': self.foto_thumb
            })
            return im.url
        else:
            return 'Sem Imagem'

    imagemAdmin.allow_tags = True
    imagemAdmin.short_description = u'Foto'