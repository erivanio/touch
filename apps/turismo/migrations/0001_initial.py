# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Lugar'
        db.create_table(u'turismo_lugar', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('thumb', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('link', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'turismo', ['Lugar'])

        # Adding model 'Foto'
        db.create_table(u'turismo_foto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('foto', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('legenda', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('foto_thumb', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('lugar', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['turismo.Lugar'])),
            ('cadastrado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'turismo', ['Foto'])


    def backwards(self, orm):
        # Deleting model 'Lugar'
        db.delete_table(u'turismo_lugar')

        # Deleting model 'Foto'
        db.delete_table(u'turismo_foto')


    models = {
        u'turismo.foto': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Foto'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'lugar': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['turismo.Lugar']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'turismo.lugar': {
            'Meta': {'object_name': 'Lugar'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.TextField', [], {}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'thumb': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['turismo']