# -*- coding: utf-8 -*-
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from apps.turismo.models import *
from image_cropping import ImageCroppingMixin
from multiupload.admin import MultiUploadAdmin

class FotoAdmin(MultiUploadAdmin):
    list_display = ('imagemAdmin', 'legenda', 'lugar')
    search_fields = ['lugar__nome']
    list_filter = ['cadastrado_em', 'lugar']
    # default value of all parameters:
    change_form_template = 'multiupload/change_form.html'
    change_list_template = 'multiupload/change_list.html'
    multiupload_template = 'multiupload/upload.html'
    multiupload_list = True
    multiupload_form = True
    multiupload_maxfilesize = 3 * 2 ** 20 # 3 Mb
    multiupload_minfilesize = 0
    multiupload_acceptedformats = ( "image/jpeg",
                                    "image/jpg",
                                    "image/png",)

    def process_uploaded_file(self, uploaded, object, request):
        '''
        This method will be called for every file uploaded.
        Parameters:
            :uploaded: instance of uploaded file
            :object: instance of object if in form_multiupload else None
            :kwargs: request.POST received with file
        Return:
            It MUST return at least a dict with:
            {
                'url': 'url to download the file',
                'thumbnail_url': 'some url for an image_thumbnail or icon',
                'id': 'id of instance created in this method',
                'name': 'the name of created file',
            }
        '''
        # example:
        lugar_id = request.POST.get('lugar_id', [''])
        title = request.POST.get('title', [''])
        f = Foto(foto=uploaded, legenda=title, lugar_id=lugar_id)
        f.save()

        return {
            'url': f.imagem(),
            'thumbnail_url': f.imagem(),
            'id': f.id,
            'name': f.legenda
        }

    def delete_file(self, pk, request):
        '''
        Function to delete a file.
        '''
        # This is the default implementation.
        obj = get_object_or_404(self.queryset(request), pk=pk)
        obj.delete()

    #def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
    #
    #    if db_field.name == 'album':
    #        kwargs['queryset'] = Foto.objects.all()
    #    else:
    #        pass
    #
    #    return super(FotoAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Foto, FotoAdmin)

class LugarAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('imagemAdmin', 'nome', 'get_actions')
    list_filter = ['nome']
    search_fields = ['nome']

    def response_add(self,request,obj,post_url_continue=None):
        return HttpResponseRedirect("/admin/turismo/foto/multiupload/?lugar_id=%s" % (obj.id))

    def get_actions(self, obj):
        try:
            return u'<div align="center"><a class="btn btn-success" href="/admin/turismo/foto/multiupload/?lugar_id=%s">Adicionar Imagens</a>  <a class="btn btn-info" href="/admin/turismo/foto/?q=&lugar__id__exact=%s">Ver Imagens</a></div>' % (obj.id, obj.id)
        except:
            return
    get_actions.short_description = 'Ações'
    get_actions.allow_tags = True

admin.site.register(Lugar, LugarAdmin)